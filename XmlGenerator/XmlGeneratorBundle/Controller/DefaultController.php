<?php

namespace XmlGenerator\XmlGeneratorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('XmlGeneratorBundle:Default:index.html.twig', array('name' => $name));
    }
}
