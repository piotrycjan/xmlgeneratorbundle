<?php

namespace XmlGenerator\XmlGeneratorBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use XmlGenerator\XmlGeneratorBundle\Services\XmlGenerator;

/**
 * Generator command line class
 * @author Piotr Szczygiel <psz.szczygiel@gmail.com>
 * > php app/console xml:generate --root-nodes=4 --tree-deep=5 --attributes=10
 */
class GenerateCommand extends Command
{
    // Variable contains input from --root-nodes
    private $root_nodes;

    // Variable contains input from --tree-deep
    private $tree_deep;

    // Variable contains input from --attributes
    private $attributes;

    // XmlGenerator instance
    private $generator;
    
    /**
     * configuration method
     */
    protected function configure()
    {
        $this
            ->setName('xml:generate')
            ->setDescription('XML files generator')
            ->addOption(
                'root-nodes',
                null,
                InputOption::VALUE_REQUIRED,
                'Number of nodes which will be generated under the root node'
            )
            ->addOption(
                'tree-deep',
                null,
                InputOption::VALUE_REQUIRED,
                'How many levels each branch will have'
            )
            ->addOption(
                'attributes',
                null,
                InputOption::VALUE_REQUIRED,
                'Number from 0-10, where 0 mens no attributes at all and 10 means lot of attributes'
            );
    }

    /**
     * Method run after executing the console script
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // collecting the input data
        $this->collectInput(
            $input->getOption( 'root-nodes' ),
            $input->getOption( 'tree-deep' ),
            $input->getOption( 'attributes' )
        );

        // generating the XML file
        $this->generateDocument();
        // printing out the statistics
        $this->printStatistics( $output );
    }

    /**
     * Method collects the input data, validates it and stores
     * @param type $root_nodes
     * @param type $tree_deep
     * @param type $attributes
     * @throws \RuntimeException
     */
    private function collectInput( $root_nodes, $tree_deep, $attributes )
    {
        if ( !filter_var( $root_nodes, FILTER_VALIDATE_INT ) || !filter_var( $tree_deep, FILTER_VALIDATE_INT ) || !filter_var( $attributes, FILTER_VALIDATE_INT ) )
        {
            throw new \RuntimeException( 'All options are required, and they need to be an integers.' );
        }

        $this->root_nodes = $root_nodes;
        $this->tree_deep = $tree_deep;
        $this->attributes = $attributes;
    }

    /**
     * Method creates the XmlGenerator instance and generates the document
     * @throws \LogicException
     */
    private function generateDocument()
    {
        $this->generator = new XmlGenerator(
            $this->root_nodes,
            $this->tree_deep,
            $this->attributes
        );

        if ( $this->generator->generate() == false )
        {
            throw new \LogicException( 'There were some problems with generating the document.' );
        }
    }

    /**
     * Method prints out the statistics of generated data
     * @param type $output
     */
    private function printStatistics( $output )
    {
        foreach ( $this->generator->getStats() as $key => $stat )
        {
            $output->writeln('<fg=black;bg=cyan>' . $stat['desc'] . ': ' . $stat['value'] . '</fg=black;bg=cyan>');
        }
    }
}