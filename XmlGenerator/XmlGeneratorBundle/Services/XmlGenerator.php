<?php

namespace XmlGenerator\XmlGeneratorBundle\Services;

/**
 * XmlGenerator core class
 * @author Piotr Szczygiel <psz.szczygiel@gmail.com>
 */
class XmlGenerator
{
    // Variable contains input from --root-nodes
    private $root_nodes;

    // Variable contains input from --tree-deep
    private $tree_deep;
    
    // Variable contains input from --attributes
    private $attributes;

    // path to dictionary text file
    const DICTIONARY_PATH = 'src/XmlGenerator/XmlGeneratorBundle/Resources/doc/dictionary.txt';

    // array containing all words from dictionary
    private static $dictionary = array();

    // array contains root nodes which are already used, so they shouldn't be used any more
    private $used_root_nodes = array();

    // path to file where the results will be saved
    const OUTPUT_FILE = 'src/XmlGenerator/XmlGeneratorBundle/Resources/doc/output.xml';

    // output file version
    const XML_VERSION = '1.0';

    // output file encoding
    const XML_ENCODING = 'ISO-8859-1';

    // array containing the statistics of generated content
    private $output_stats = array(
        'nodes'         => array(
            'value' => 0,
            'desc'  => 'Number of unique nodes generated'
        ),
        'attributes'    => array(
            'value' => 0,
            'desc'  => 'Number of unique attributes generated'
        )
    );

    /**
     * Cunstructor
     * @param int $root_nodes
     * @param int $tree_deep
     * @param int $attributes
     */
    public function __construct( $root_nodes = 0, $tree_deep = 0, $attributes = 0 )
    {
        $this->setRootNodes( $root_nodes );
        $this->setTreeDeep( $tree_deep );
        $this->setAttributes( $attributes );
    }

    /**
     * Setter for root nodes
     * @param int $root_nodes
     */
    public function setRootNodes( $root_nodes )
    {
        $this->root_nodes = $root_nodes;
    }

    /**
     * Setter for tree deep
     * @param int $tree_deep
     */
    public function setTreeDeep( $tree_deep )
    {
        $this->tree_deep = $tree_deep;
    }

    /**
     * Setter for attributes
     * @param int $attributes
     */
    public function setAttributes( $attributes )
    {
        $this->attributes = $attributes;
    }

    /**
     * Main method. In general - it generates the xml file :)
     * @return int the number of bytes written or <b>FALSE</b> if an error occurred.
     */
    public function generate()
    {
        // reading the dictionary into memory
        self::readDictionary();
        // new document instance
        $document = new \DOMDocument( self::XML_VERSION, self::XML_ENCODING );

        // creating the root node
        $root_node = $document->createElement( $this->getTagName() );
        $branch = $root_node;
        $first_branch = null;

        // creating the branch will, basing on "tree-deep" value
        for( $j = 0; $j < $this->tree_deep; $j++ )
        {   
            $children_node_name = $this->getTagName();

            // condition for all nodes, except last one - with value
            if ( $j < $this->tree_deep - 1 )
            {
                $node = $document->createElement( $children_node_name );
                $branch = $this->appendChild( $branch, $node );
                // creating the attributes
                for( $k = 0; $k < $this->attributes; $k++ )
                {
                    $this->setAttribute( $branch, self::getRandomWord(), self::getRandomWord() );
                }
            }
            // this part goes for last node, containing the text value
            else
            {
                // last node is repeated. I took the "tree-deep" value as a base for this.
                for( $i = 0; $i < $this->tree_deep; $i++ )
                {
                    $node = $document->createElement( $children_node_name, self::getRandomWord() );
                    // creating the attributes
                    for( $k = 0; $k < $this->attributes; $k++ )
                    {
                        $this->setAttribute( $node, self::getRandomWord(), self::getRandomWord() );
                    }
                    $this->appendChild( $branch, $node );
                }
            }

            // we need to store first branch, so we could clone it later on
            if ( $j === 0)
            {
                $first_branch = $branch;
            }
        }

        // creating the nodes under root node - which number cames from 'root-nodes' option
        if ( !is_null( $first_branch ) )
        {
            for( $i = 0; $i < $this->root_nodes; $i++ )
            {
                // this is made by clonning the branch generated above
                $this->appendChild( $root_node, $first_branch->cloneNode( $this->tree_deep ) );
            }
        }

        // in this point all is appended to the document
        $this->appendChild( $document, $root_node );
        // ... and document is saved
        return $document->save( self::OUTPUT_FILE );
    }

    /**
     * Method reads the dictionary file and stores it in the object
     */
    private static function readDictionary()
    {
        self::$dictionary = file( self::DICTIONARY_PATH );
    }

    /**
     * Method returns random tag name, which also is stored into used_root_nodes array
     * @throws \OutOfBoundsException
     * @return string
     */
    private function getTagName()
    {
        if ( count( self::$dictionary ) == count( $this->used_root_nodes ) )
        {
            throw new \OutOfBoundsException( 'We\'re out of dictionary words. You should extend your dictionary or decrease the number of root-nodes.' );
        }

        $new_tag = '';
        while( $new_tag == '' )
        {
            $tag = self::getRandomWord();
            if ( !in_array( $tag, $this->used_root_nodes ) )
            {
                $new_tag = $tag;
                $this->used_root_nodes[] = $new_tag;
            }
        }

        return $new_tag;
    }

    /**
     * Method returns random tag name
     * @return string
     */
    private static function getRandomWord()
    {
        $index = rand( 0, count( self::$dictionary ) -1 );
        return trim( self::$dictionary[$index] );
    }

    /**
     * Function overries the DOMNode::appendChild method. The difference is that here we can do the counting of added elements.
     * @param DOMNode $branch
     * @param DOMNode $node
     * @return DOMNode
     */
    private function appendChild( $branch, $node )
    {
        $this->output_stats['nodes']['value']++;
        return $branch->appendChild( $node );
    }

    /**
     * Function overries the DOMElement::setAttribute method. The difference is that here we can do the counting of added attributes.
     * @param DOMNode $branch
     * @param string $name
     * @param string $value
     * @return DOMNode
     */
    private function setAttribute( $branch, $name, $value )
    {
        $this->output_stats['attributes']['value']++;
        return $branch->setAttribute( $name, $value );
    }

    /**
     * Getter for array with statistics
     * @return type
     */
    public function getStats()
    {
        return $this->output_stats;
    }
}